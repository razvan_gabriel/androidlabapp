# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the repository for the Android class

### Quick links ###

* [Class website](https://sites.google.com/site/fiiandroidprogramming/home)
* [Marks site](https://docs.google.com/spreadsheets/d/1rjvcl0FIZpQmELPS8-fcFE78x9FH64WsASCIN3Wt7QY/edit?fbclid=IwAR19Fp908Blw0h4r5uSF8tKkUeI0XR1eseOipD-blnIGOJVDr0fiKAqLm1k#gid=0)

### Set up ###

* I used a Nexus 4 API 23 simulator.

### Who do I talk to? ###

* Contact me by [email](mailto:sylvergabriel@gmail.com)