package com.example.laborator1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ExampleDialog.ExampleDialogListener {
    private static final String TAG = "MainActivity";

    private TextView mTextViewCount;
    private int mCount;


    //initialize the dialog
    private TextView textViewUsername;
    private TextView textViewPassword;
    private Button openDialogBtn;

    //initialize settings
    Button settings;
    TextView music;
    TextView position;

    boolean b_music;
    int b_position;


    //initialize sensors activity
    private Button openSensors;


    //initialize camera activity
    private Button openCamera;



    ListView listView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(TAG, "onCreate");


        //initialize the counter and the buttons
        mTextViewCount = findViewById(R.id.counter);
        Button buttonIncrement = findViewById(R.id.btnIncrement);


        buttonIncrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increment();
            }
        });

        listView = (ListView)findViewById(R.id.listview);


        final ArrayList<String> arrayList=new ArrayList<>();

        arrayList.add("Website design");
        arrayList.add("SEO improvements");
        arrayList.add("MVP development");


        ArrayAdapter arrayAdapter=new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);

        listView.setAdapter(arrayAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this,"clicked item on position " + (position+1) + " with the name: " + arrayList.get(position).toString(), Toast.LENGTH_SHORT).show();
            }
        });


        if(savedInstanceState != null) {
            mCount = savedInstanceState.getInt("count");
            mTextViewCount.setText(String.valueOf(mCount));
        }

        textViewUsername = (TextView) findViewById(R.id.textview_username);
        textViewPassword = (TextView) findViewById(R.id.textview_password);
        openDialogBtn = (Button) findViewById(R.id.buttonDialog);
        openDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });


        openSensors = findViewById(R.id.btnSensors);

        openSensors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SensorsActivity.class));
            }
        });


        openCamera = findViewById(R.id.btnCamera);

        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CameraActivity.class));
            }
        });




        music = findViewById(R.id.settingsRead);
        settings = findViewById(R.id.buttonSettings);
        position = findViewById(R.id.position);

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

        SharedPreferences settings = getSharedPreferences("PREFS", MODE_PRIVATE);
        b_music = settings.getBoolean("MUSIC", true);
        b_position = settings.getInt("POSITION", 0);

        if(b_music){
            music.setText("Music ON");
        } else {
            music.setText("Music OFF");
        }

        position.setText("Position: "+b_position);

    }

    //local storage function
    private boolean isExternalStorageWritable(){
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            Log.i("State", "Yes, it's writable");
            return true;
        }else{
            return false;
        }
    }

    public void writeFile(View v){
        if(isExternalStorageWritable() && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            File textFile = new File(Environment.getExternalStorageDirectory(), "settings.txt");
            try{
                FileOutputStream fos = new FileOutputStream(textFile);
                fos.write(textViewUsername.getText().toString().getBytes());
                fos.close();

                Toast.makeText(this, "File saved.", Toast.LENGTH_SHORT).show();
            }catch (IOException e){
                e.printStackTrace();
            }
        }else {
            Toast.makeText(this, "Cannot write to external storage.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkPermission(String permission){
        int check = ContextCompat.checkSelfPermission(this, permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }


    public void openDialog() {
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.show(getSupportFragmentManager(), "example dialog");
    }

    @Override
    public void applyTexts(String username, String password) {
        textViewUsername.setText(username);
        textViewPassword.setText(password);
    }

    private void increment() {
        mCount++;
        mTextViewCount.setText(String.valueOf(mCount));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
//                Toast.makeText(this, "Item one selected", Toast.LENGTH_SHORT).show();
                openActivity2();
                return true;

            case R.id.item2:
                Toast.makeText(this, "Second item selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.item3:
                Toast.makeText(this, "Third item selected", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    public void openActivity2() {
//        Intent intent = new Intent(this, Activity2.class);
//        Intent chooser = Intent.createChooser(intent, "send intent title");
//        if(intent.resolveActivity(getPackageManager()) != null) {
//            startActivity(intent);
//        }
//
//    }

    public void openActivity2() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, "this is only a test");
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Send text to..."));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("count", mCount);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i(TAG, "onResume");
        SharedPreferences settings = getSharedPreferences("PREFS", MODE_PRIVATE);
        b_music = settings.getBoolean("MUSIC", true);
        b_position = settings.getInt("POSITION", 0);

        if(b_music){
            music.setText("Music ON");
        } else {
            music.setText("Music OFF");
        }

        position.setText("Position: "+b_position);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i(TAG, "onStop");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.i(TAG, "onRestart");
    }




}
