package com.example.laborator1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;

public class SettingsActivity extends AppCompatActivity {

    CheckBox music;
    SeekBar position;

    boolean b_music;
    int b_position;

    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        music = findViewById(R.id.music);
        position = findViewById(R.id.position);

        settings = getSharedPreferences("PREFS", MODE_PRIVATE);

        b_music = settings.getBoolean("MUSIC", true);
        b_position = settings.getInt("POSITION", 0);

        if (b_music){
            music.setChecked(true);
        }

        position.setProgress(b_position);

        music.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean b) {
                if(b){
                    b_music = true;
                    saveSettingsBoolean("MUSIC", b_music);
                } else {
                    b_music = false;
                    saveSettingsBoolean("MUSIC", b_music);
                }
            }
        });

        position.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                saveSettingsInt("POSITION", i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void saveSettingsBoolean(String s, boolean b){
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(s, b);
        editor.apply();
    }

    public void saveSettingsInt(String s, int i){
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(s, i);
        editor.apply();
    }
}
