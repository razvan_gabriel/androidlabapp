package com.example.laborator1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String message = bundle.getString(Intent.EXTRA_TEXT);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
